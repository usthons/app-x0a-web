<?php
	$DB_NAME = "kampus";
	$DB_USER = "root";
	$DB_PASS = "";
	$DB_SERVER_LOC = "locahost";

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT m.nim, m.nama, p.nama_prodi, m.alamat, concat('http://192.168.43.140/kampus/images/',m.photos) as url
            FROM mahasiswa m, prodi p 
            WHERE m.id_prodi = p.id_prodi";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Acces-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");
            $data_mahasiswa = array();
            while($mhs = mysqli_fetch_assoc($result)){
                array_push($data_mahasiswa,$mhs);
            }
            echo json_encode($data_mahasiswa);
        }
    }
    

?>