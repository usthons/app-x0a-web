<?php
    $DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT m.nim, m.nama, p.nama_prodi, m.alamat, m.photos
            FROM mahasiswa m, prodi p 
            WHERE m.id_prodi = p.id_prodi";
    
    $result = mysqli_query($conn,$sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <h4>Data Mahasiswa</h4>
            <table class="mt-3 table table-stripped">
                <tr>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Prodi</th>
                    <th>Alamat</th>
                    <th>Foto</th>
                </tr>
                <?php 
                    while($mhs = mysqli_fetch_assoc($result)){
                ?>
                <tr>
                <td><?php echo $mhs['nim']; ?></td>
                    <td><?php echo $mhs['nama']; ?></td>
                    <td><?php echo $mhs['nama_prodi']; ?></td>
                    <td><?php echo $mhs['alamat']; ?></td>
                    <td><img src="images/<?php echo $mhs['photos']; ?>" style="width: 100px;" alt="Foto Mahasiswa"></td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>